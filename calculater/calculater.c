/**
 * 一个简单的计算器
 * 主要将一行计算式的结果输出
 * 主要使用了中缀表达式转后缀表达式
 *
 * 实现了一个简单的栈(stack)
 * 并将计算式中的元素抽象成一个个带tag的token
 * 通过后缀表达式计算输出结果
 */

#include<string.h>
#include<stdio.h>
#include<stdlib.h>

#define MAX_STACK_SIZE 1000
#define MAX_WORD_SIZE 50
#define MAX_LINE_LENGTH 100
 /*
	 SIGN: 计算符
 */
enum Tag {
	START, END, SIGN, INT, FLOAT,IDENTIFER_F,IDENTIFER_I,IDENTIFER_W,VAL,RETURN,FINAL,GIVEVALUE
};
//枚举类型定义宏定义
/**
 * 加减乘除
 * LP: 左括号（Left parenthesis）
 * RP: 右括号（Right parenthesis）
 */
enum Sign {
	PLUS = '+', MINUS = '-', MUL = '*', DIV = '/', LP = '(', RP = ')'
};

/**
 *储存变量的和值的双链表结构
 */
typedef struct _valItem
{

	struct valItem* front;
	struct valItem* back;
	int type;//type为0 表示float   //type为1 表示int
	char val[MAX_WORD_SIZE];
	union Value
	{
		float float_val;
		int int_val;
	} value;

} valItem;





/**
 * token相关数据结构和函数
 * tag表示token类型
 * value表示具体值
 */
typedef struct _Token
{
	int tag;
	union value
	{
		int sign;
		int i_val;
		float f_val;
	} value;

} Token;

/**
 * 栈相关数据结构和函数
 */
typedef struct _Stack
{
	Token* top;
	Token* base;
	int size;
}Stack;

void init_stack(Stack* stack);
void free_stack(Stack* stack);
void push(Stack* stack, Token t);
Token* pop(Stack* stack);
Token* top(Stack* stack);
void inverse(Stack* stack);

/**
 * reset:
 * scan: 扫描字符串，获取下一个token
 * cal: 计算给定表达式的结果
 * error: 输出错误信息并退出
 * cal_token: 计算两个token二元运算的结果，sign为运算符，结果输出到result参数里
 * get_op_level:获取操作符运算优先级
 */
int scan(char* expr, int start, Token* current, char* val);
valItem* cal(char* expr, valItem* head);
void error(char* message);
void cal_token(Token* t1, Token* t2, int sign, Token* result);
int get_op_level(int op);

/**
 * 变量链表的相关函数
 */
void init_valLink(valItem* valitem);
valItem findVal(valItem* valitem, char* valName);
valItem findValPtr(valItem* valitem, char* valName);
valItem* addIntVal(valItem* valitem, char* valName, int num);
valItem* addFloatVal(valItem* valitem, char* valName, float num);
valItem* findPtr(valItem* valitem, char* valName);


int LINE = 0;
char* BUF = NULL;

int main() {
	//char* PATH="D://test.txt";
	char* PATH = malloc(MAX_LINE_LENGTH);
	memset(PATH, '\0', MAX_LINE_LENGTH);
	printf("请输入path：");
	scanf("%s", PATH);  /*输入参数是已经定义好的“字符数组名”, 不用加&, 因为在C语言中数组名就代表该数组的起始地址*/
	printf("输出结果：%s\n", PATH);
	FILE* f;
	char buf[MAX_LINE_LENGTH];
	int len;
	if ((f = fopen(PATH, "r")) == NULL)
	{
		error("fail to read");
	}
	valItem valDoubleLink;
	valDoubleLink.back = NULL;
	valDoubleLink.front = NULL;
	strcpy(valDoubleLink.val, "#");
	valItem* head;
	valItem* h;
	head = &valDoubleLink;
	while (fgets(buf, MAX_LINE_LENGTH, f) != NULL)
	{
		++LINE;
		BUF = buf;
		len = strlen(buf);
		buf[len - 1] = '\0';  /*去掉换行符*/
		//printf("%s %d \n", buf, len - 1);
		h=cal(buf, head);
		head = h;
	}
	system("pause");
	return 0;
}

valItem findVal(valItem* valitem, char* valName)
{
	valItem* find = valitem;
	valItem result;
	while (strcmp(find->val, "#") != 0)
	{
		if (strcmp(find->val, valName) == 0)//找到变量
		{
			result.type = find->type;
			if (find->type == 0) {
				result.value.float_val = find->value.float_val;
			}
			if (find->type == 1) {
				result.value.int_val = find->value.int_val;
			}
			return result;
		}
		else
		{
			find = find->back;
		}
	}
	//未找到变量
	error("undefined value");
}
valItem* findPtr(valItem* valitem, char* valName) {
	valItem* p = valitem;
	while (strcmp(p->val, "#") != 0)
	{
		if (strcmp(p->val, valName) == 0)//找到变量
		{
			return p;
		}
		else
		{
			p = p->back;
		}
	}
	//未找到变量
	error("undefined value");
}
valItem findValPtr(valItem* valitem, char* valName) {
	valItem* find = valitem;
	valItem result;
	while (strcmp(find->val, "#")!=0)
	{
		if (strcmp(find->val,valName)==0)//找到变量
		{
			result.type = find->type;
			if (find->type == 0) {
				result.value.float_val = find->value.float_val;
			}
			if (find->type == 1) {
				result.value.int_val = find->value.int_val;
			}
			return result;
		}
		else
		{
			find = find->back;
		}
	}
	//未找到变量
	error("undefined value");
}
valItem* addFloatVal(valItem* valitem, char* valName, float num)
{
	valItem* newItem = (valItem*)malloc(sizeof(valItem));
	strcpy(newItem->val, valName);
	newItem->value.float_val = num;
	newItem->type = 0;
	newItem->back = valitem;
	newItem->front = NULL;
	valitem->front = newItem;
	valitem = newItem;
	return valitem;
}


valItem* addIntVal(valItem* valitem, char* valName, int num)
{
	valItem* newItem= (valItem*)malloc(sizeof(valItem));
	newItem->value.int_val = num;
	newItem->type = 1;
	strcpy(newItem->val, valName);
	newItem->back = valitem;
	newItem->front = NULL;
	valitem->front = newItem;
	valitem = newItem;
	return valitem;
}

void init_valLink(valItem* valitem)
{
	valItem item;
	item.back = NULL;
	item.front = NULL;
	strcpy(item.val, "#");
	valitem = &item;
}

int get_op_level(int op) {
	switch (op)
	{
	case '(':
		return 1;
	case '+':
	case '-':
		return 2;
	case '*':
	case '/':
		return 3;
	case ')':
		return 4;
	default:
		error("bad op");
		return 0;
	}
}

// ---------------------- 相关函数的实现 ----------------------

/**
 * 栈相关
 */
void init_stack(Stack* stack) {
	stack->base = malloc(MAX_STACK_SIZE * sizeof(Stack));
	stack->size = 0;
	stack->top = NULL;
}

void free_stack(Stack* stack) {
	free(stack->base);
}

void push(Stack* stack, Token t) {
	int size = stack->size;

	if (size >= MAX_STACK_SIZE)
	{
		error("stack overflow");
	}

	stack->base[size] = t;
	stack->size++;
	stack->top = stack->base + stack->size - 1;
}

Token* pop(Stack* stack) {
	if (stack->size == 0)
	{
		error("can't pop empty stack");
	}

	Token* top_token = top(stack);
	stack->top = stack->top - 1;
	stack->size--;
	return top_token;
}

Token* top(Stack* stack) {
	return stack->top;
}
//将栈中元素的顺序翻转
void inverse(Stack* stack) {
	Stack s2;
	init_stack(&s2);

	for (size_t i = stack->size; i > 0; i--)
	{
		Token* t = pop(stack);
		push(&s2, *t);
	}
	free_stack(stack);
	stack->top = s2.top;
	stack->base = s2.base;
	stack->size = s2.size;
}

/**
 * 计算表达式相关函数
 * 主要使用的方法是中缀表达式转后缀表达式，再计算后缀表达式
 */
valItem* cal(char* expr,valItem* head) {
	valItem* p=head;
	int len = strlen(expr);
	int location = 0; // 下标
	Token current;
	Token current_1;
	Token* _current=(Token*)malloc(sizeof(Token));
	// 167-252行：中缀转后缀
	Stack post;
	Stack ops;

	//赋值语句标记
	int is_giveValue = 0;//0代表不是
	//标记赋值变量
	int is_foundValue = 0;
	//是否进行scan
	int valueInFunc = 0;
	//赋值变量名
	char* name_val = NULL;

	current.tag = START;

	init_stack(&post);
	init_stack(&ops);
	char* val = malloc(MAX_WORD_SIZE);
	char* val_1 = malloc(MAX_WORD_SIZE);;
	valItem value_struct;

	//用于write标记变量名和变量结构
	char* val_name = NULL;
	valItem val_name_struct;

	while (current.tag != END)
	{
		if (current.tag == RETURN) {
			break;
			//return p;
		}
		if (valueInFunc != 1) {
			location = scan(expr, location, &current, val);
		}
		else {
			valueInFunc = 0;
		}
		switch (current.tag)
		{
		case VAL:
			findVal(head, val);
			if (is_foundValue == 0) {
				name_val = malloc(strlen(val));
				memset(name_val, '\0', strlen(val));
				is_foundValue = 1;
			}
			location = scan(expr, location, &current, val_1);
			if (current.value.sign == '=') {
				is_giveValue = 1;
				if (name_val[0] != '\0') {
					error("error grammar");
				}
				strcpy(name_val, val);
				break;
			}
			else if(current.tag == SIGN && current.value.sign != '='){
				value_struct = findVal(head, val);
				if (value_struct.type == 1) {
					_current->tag = INT;
					_current->value.i_val = value_struct.value.int_val;
				}
				else if (value_struct.type == 0) {
					_current->tag = FLOAT;
					_current->value.f_val = value_struct.value.float_val;
				}
				push(&post, *_current);
				valueInFunc = 1;
				//与对照表对应查找val的值然后做push(&post, current);操作
				//清空val
			}
			break;
		case START:
			//unexpected case
			error("scan error: find no token");
			break;
		case END:
			break;
		case RETURN:
			break;
			//return head;
		case FINAL:
			printf("DONE");
			system("pause");
			return head;
		case INT:
		case FLOAT:
			push(&post, current);
			break;
		//如果发现保留字就继续扫描下一个token，也就是变量
		case IDENTIFER_F:
			location = scan(expr, location, &current, val);
			if (current.tag != VAL)
			{
				error("wrong value name");
			}
			else {
				char* mark=malloc(strlen(val));
				strcpy(mark, val);
				do {
					location = scan(expr, location, &current, val);
					if (current.tag == SIGN && current.value.sign == '=') {
						location = scan(expr, location, &current, val);
						if (current.tag != FLOAT) {
							error("wrong grammar");
						}
						else {
							p=addFloatVal(head, mark, current.value.f_val);
							break;
						}
					}
					else if(current.tag == END || current.tag == RETURN){
						p=addFloatVal(head, mark, 0.0);
						break;
					}
					else {
						error("wrong grammar");
					}
				} while (1);
			}
			break;
		case IDENTIFER_W:
			
			location = scan(expr, location, &current, val);
			if (current.value.sign != '(') {
				error("error in (write) function");
			}
			location = scan(expr, location, &current, val);
			if (current.tag != VAL) {
				error("error in (write) function");
			}
			val_name = malloc(strlen(val));
			strcpy(val_name, val);

			location = scan(expr, location, &current, val);
			if (current.value.sign != ')') {
				error("error in (write) function");
			}
			val_name_struct=findValPtr(head, val_name);
			/*if (val_name_struct == NULL) {
				error("the value cannot be found");
			}*/
			if (val_name_struct.type == 0) {
				printf("%f\n", val_name_struct.value.float_val);
			}
			if (val_name_struct.type == 1) {
				printf("%d\n", val_name_struct.value.int_val);
			}
			break;
		case IDENTIFER_I:
			location = scan(expr, location, &current,val);
			if (current.tag != VAL)
			{
				error("wrong value name");
			}
			else {
				char* mark = malloc(strlen(val));
				strcpy(mark, val);
				do {
					location = scan(expr, location, &current, val);
					if (current.tag == SIGN && current.value.sign == '=') {
						location = scan(expr, location, &current, val);
						if (current.tag != INT) {
							error("wrong grammar");
						}
						else {
							p=addIntVal(head, mark, current.value.i_val);
							break;
						}
					}
					else if (current.tag == END || current.tag == RETURN) {
						p=addIntVal(head, mark, 0);
						break;
					}
					else {
						error("wrong grammar");
					}
				} while (1);
			}
			//*************************************
			break;
		case SIGN: {
			int sign = current.value.sign;
			switch (sign)
			{
			case LP:
				push(&ops, current);
				break;
			case RP:
				do
				{
					Token* op;
					if (ops.size == 0)
					{
						error("parentheses mismatch");
					}
					op = pop(&ops);
					push(&post, *op);

				} while (top(&ops)->value.sign != LP);
				pop(&ops);
				break;
			case PLUS:
			case MINUS:
			case MUL:
			case DIV: {
				if (ops.size == 0)
				{
					push(&ops, current);
					break;
				}
				int top_op = top(&ops)->value.sign;
				if (get_op_level(sign) > get_op_level(top_op))
				{
					push(&ops, current);
					break;
				}
				else {
					while (ops.size != 0)
					{
						int top_op = top(&ops)->value.sign;
						Token* op;
						if (get_op_level(sign) <= get_op_level(top_op))
						{
							op = pop(&ops);
							push(&post, *op);
						}
						else {
							break;
						}
					}
					push(&ops, current);
					break;
				}
			}
			default:
				break;
			}
			break;
		}
		default:
			//unexpected case
			error("scan error: find unexpected token");
			break;
		}


	}
	

	//是赋值表达式
	if (is_giveValue == 1) {
		
		while (ops.size != 0)
		{
			Token* op = pop(&ops);
			push(&post, *op);
		}

		free_stack(&ops);

		// 转换的后缀里的元素是相反的，所以需要反转栈中数据
		inverse(&post);
		// 计算后缀表达式
		Stack tmp;
		init_stack(&tmp);

		while (post.size != 0) {
			int tag = top(&post)->tag;
			switch (tag)
			{
			case INT:
			case FLOAT:
				push(&tmp, *pop(&post));
				break;
			case SIGN: {
				Token t;
				int sign = pop(&post)->value.sign;
				switch (sign) {
				case '+':
				case '-':
				case '*':
				case '/': {
					Token* value2 = pop(&tmp);
					Token* value1 = pop(&tmp);
					cal_token(value1, value2, sign, &t);
					push(&tmp, t);
					break;
				}
				}
				break;
			}
			default:
				error("aaa");
				break;
			}
		}
		if (tmp.size != 1) {
			//Token* cal_result1 = pop(&tmp);
			//Token* cal_result2 = pop(&tmp);

			//printf("%d\n", cal_result1->value.i_val);
			//printf("%d\n", cal_result2->value.i_val);
			printf("%d\n", tmp.size);
			error("get values");
		}

		// 计算的结果
		Token* cal_result = pop(&tmp);
		valItem* result = findPtr(head, name_val);
		if (cal_result->tag == FLOAT) {
			if (result->type == 1) {
				int resultInt = cal_result->value.f_val;
				result->value.int_val = resultInt;
			}
			else {
				result->value.float_val = cal_result->value.f_val;
			}
		}
		else if (cal_result->tag == INT) {
			if (result->type == 0) {
				float resultFloat = cal_result->value.f_val;
				result->value.float_val = resultFloat;
			}
			else {
				result->value.int_val = cal_result->value.i_val;
			}
			
		}
		//if (cal_result->tag == INT)
		//{
		//	//printf("%d\n", cal_result->value.i_val);
		//}
		//else if (cal_result->tag == FLOAT) {
		//	//printf("%.2f\n", cal_result->value.f_val);
		//}
	}
	return p;

	

}

int scan(char* expr, int start, Token* current,char* val) {
	int len = strlen(expr);
	int offset = start;
	char curr_char = expr[offset];
	while (offset < len)
	{
		// 跳过空格
		if (curr_char == ' ')
		{
			curr_char = expr[++offset];
			continue;
		}
		//发现分号结束
		if (curr_char == ';') {
			current->tag = RETURN;
			return offset + 1;
		}
		if (curr_char == '.') {
			current->tag = FINAL;
			return offset + 1;
		}
		if (curr_char == '\0')
		{
			current->tag = END;
			return offset + 1;
		}

		// 解析计算符
		if (curr_char == '+' || curr_char == '-' || curr_char == '*' || curr_char == '/'
			|| curr_char == '(' || curr_char == ')')
		{
			current->tag = SIGN;
			current->value.sign = curr_char;
			return offset + 1;
		}
		//解析赋值 =
		if (curr_char == '=') 
		{
			current->tag = SIGN;
			current->value.sign = curr_char;
			return offset + 1;
		}

		// 解析数字
		if (curr_char >= '0' && curr_char <= '9')
		{
			int is_float = 0;
			int int_val = 0;
			float float_val;

			do
			{
				int_val = int_val * 10 + (curr_char - '0');
				curr_char = expr[++offset];
			} while (curr_char >= '0' && curr_char <= '9');

			// 如果有小数点，则为float
			if (curr_char == '.')
			{
				float_val = int_val;
				curr_char = expr[++offset];
				float times = 10.0f;

				int overflow = 0;
				while (curr_char >= '0' && curr_char <= '9') {
					if (overflow)
					{
						curr_char = expr[++offset];
						continue;
					}

					float_val += (curr_char - '0') / times;
					curr_char = expr[++offset];
					times *= 10;
					// 防止溢出
					if (times > 0xffffffffu / 10)
					{
						overflow = 1;
					}
				}

				current->tag = FLOAT;
				current->value.f_val = float_val;
				return offset;
			}

			current->tag = INT;
			current->value.i_val = int_val;
			return offset;
		}
		if ((curr_char >= 'A' && curr_char <= 'z'))
		{
			char* word=malloc(MAX_WORD_SIZE);
			memset(word, 0, MAX_WORD_SIZE);
			char _char = "a";
			int word_len = 0;
			while (curr_char != ';' && curr_char != ' ' && curr_char != '\0'&& curr_char != '('&& curr_char != ')') {
				//strcpy(_char, curr_char);
				_char = curr_char;
				/*word[word_len] = _char;*/
				*(word+word_len)= _char;
				word_len++;
				if (word_len > MAX_WORD_SIZE)
				{
					error("beyond namespace");
				}
				curr_char = expr[++offset];
			}
			if (strcmp(word, "float") == 0)
			{
				current->tag = IDENTIFER_F;
				return offset;
			}
			else if (strcmp(word, "int") == 0)
			{
				current->tag = IDENTIFER_I;
				return offset;
			}
			else if (strcmp(word, "write") == 0) {
				current->tag = IDENTIFER_W;
				return offset;
			}
			else {
				current->tag = VAL;
				strcpy(val, word);
				return offset;
			}
			error("bad input");
		}

	}
	if (curr_char == '\0')
	{
		current->tag = END;
		return offset;
	}
	//**************************
	

	//**************************
}


void cal_token(Token* t1, Token* t2, int sign, Token* result) {
	if (t1->tag == FLOAT || t2->tag == FLOAT)
	{
		result->tag = FLOAT;
		float v1, v2;
		v1 = t1->tag == FLOAT ? t1->value.f_val : t1->value.i_val;
		v2 = t2->tag == FLOAT ? t2->value.f_val : t2->value.i_val;

		switch (sign)
		{
		case '+':
			result->value.f_val = v1 + v2;
			return;
		case '-':
			result->value.f_val = v1 - v2;
			return;
		case '*':
			result->value.f_val = v1 * v2;
			return;
		case '/':
			if (v2 == 0.0f)
			{
				error("divide by zero");
			}
			result->value.f_val = v1 / v2;
			return;
		default:
			return;
		}
	}
	else {
		result->tag = INT;
		int v1, v2;
		v1 = t1->value.i_val;
		v2 = t2->value.i_val;
		switch (sign)
		{
		case '+':
			result->value.i_val = v1 + v2;
			return;
		case '-':
			result->value.i_val = v1 - v2;
			return;
		case '*':
			result->value.i_val = v1 * v2;
			return;
		case '/':
			if (v2 == 0)
			{
				error("divide by zero");
			}
			result->value.i_val = v1 / v2;
			return;
		default:
			return;
		}
	}
}

// 错误消息
void error(char* message) {
	printf("%s\n", BUF);
	fprintf(stderr, "(line%d with error) %s\n",LINE, message);
	system("pause");
	exit(1);
}